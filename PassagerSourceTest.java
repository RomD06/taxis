import java.util.List;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import junit.framework.TestCase;

/**
 * The test class PassagerSourceTest.
 *
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class PassagerSourceTest extends TestCase
{
    private PassagerSource source;
    
    /**
     * Default constructor for test class PassengerSourceTest
     */
    public PassagerSourceTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    protected void setUp()
    {
        TaxiCompany company = new TaxiCompany();
        source = new PassagerSource(company);
        Position taxiPosition = new Position(0, 0);
        Taxi taxi = new Taxi(company, taxiPosition);
        List<Vehicule> vehicules = company.getVehicules();
        vehicules.add(taxi);
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    protected void tearDown()
    {
        source = null;
    }

    /**
     * Teste du succès de la prise en charge d'un passager.
     */
    @Test
    public void testPriseEnCharge()
    {
        assertEquals(true, source.demandeCourse());
    }
}
